import { Component, OnInit, Injectable } from '@angular/core';
import { DiceSimulatorService } from '../../service/dice-simulator.service'
import { BehaviorSubject } from 'rxjs'

@Component({
  selector: 'app-roll-simulator',
  templateUrl: './roll-simulator.component.html',
  styleUrls: ['./roll-simulator.component.scss']
})
@Injectable({
  providedIn: 'root'
})
export class RollSimulatorComponent implements OnInit {

  diceSidesOptions = [4, 6, 8, 10, 12, 20, 100];
  selectedDiceSides = 6;
  selectedRollCount = 100;
  selectedDiceCount = 3;

  inputBounds = {
    minRollCount: 1,
    maxRollCount: 100,
    minDiceCount: 1,
    maxDiceCount: 100
  }
  rollDistributionResults : any[] = []
  rollDistributionResults$: BehaviorSubject<any[]> = new BehaviorSubject([])

  constructor(
    private diceSimulatorService:DiceSimulatorService
  ) { }

  ngOnInit(): void {
    
  }

  simulateRoll() {
    console.log(this.selectedDiceCount, this.selectedDiceSides, this.selectedRollCount)
    if (
        !(this.validateInputBounds(this.selectedDiceCount, 
            this.inputBounds.minDiceCount, this.inputBounds.maxDiceCount)) ||
        !(this.validateInputBounds(this.selectedRollCount, 
          this.inputBounds.minRollCount, this.inputBounds.maxRollCount))
        ) {
              console.log("incorrect inputs")
              return;
      }

      this.callRestSimulatorClient();
  }

  validateInputBounds(a, lowerBound, upperBound) {
    if (a >= lowerBound && a <= upperBound) {
      return true;
    }
  }

  callRestSimulatorClient() {
    console.log("calling simulator client")

        this.diceSimulatorService.callDiceSimulator(this.selectedDiceCount, this.selectedDiceSides, this.selectedRollCount).subscribe(
            response => {
              this.rollDistributionResults = response;
              this.rollDistributionResults$.next(this.rollDistributionResults);
            }
          )
  }

}
