import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RollDistributionComponent } from './roll-distribution.component';

describe('RollDistributionComponent', () => {
  let component: RollDistributionComponent;
  let fixture: ComponentFixture<RollDistributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RollDistributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RollDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
