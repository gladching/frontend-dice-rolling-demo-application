import { Component, OnInit } from '@angular/core';
import { DiceSimulatorService } from 'src/app/service/dice-simulator.service';

@Component({
  selector: 'app-roll-distribution',
  templateUrl: './roll-distribution.component.html',
  styleUrls: ['./roll-distribution.component.scss']
})
export class RollDistributionComponent implements OnInit {

  diceSidesOptions = [4, 6, 8, 10, 12, 20, 100];
  selectedDiceSides = 6;
  selectedDiceCount = 3;

  rollDistributionResults: any[] = []

  constructor( 
    private diceSimulatorService:DiceSimulatorService
  ) { }

  ngOnInit(): void {
  }
  
  getRollDistributionPercentages() {
    this.diceSimulatorService.retrieveDistributionPercentages(this.selectedDiceCount, this.selectedDiceSides).subscribe(
      response => {
        this.rollDistributionResults = response
      }
    )
  }
}
