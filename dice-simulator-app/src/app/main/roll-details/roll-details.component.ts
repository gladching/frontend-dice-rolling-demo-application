import { Component, OnInit } from '@angular/core';
import { DiceSimulatorService } from 'src/app/service/dice-simulator.service';
import { RollSimulatorComponent } from '../roll-simulator/roll-simulator.component';

@Component({
  selector: 'app-roll-details',
  templateUrl: './roll-details.component.html',
  styleUrls: ['./roll-details.component.scss']
})
export class RollDetailsComponent implements OnInit {
  totalRollDetails: any[] = [];

  constructor(
    private diceSimulatorService:DiceSimulatorService,
    private rollSimulatorComponent:RollSimulatorComponent
  ) { }
  
  ngOnInit(): void {
    this.rollSimulatorComponent.rollDistributionResults$.asObservable().subscribe(
      response => {
        if (response.length != 0) {
          this.getTotalRollsDetails()
        }
      }
    )
  }

  getTotalRollsDetails() {
    console.log("Calling get total roll details"); 
    this.callDataService()
  }

  callDataService() {
    this.diceSimulatorService.retrieveRollDetails().subscribe(
      response => {
        this.totalRollDetails = response
      }
    ) 
  }



}
