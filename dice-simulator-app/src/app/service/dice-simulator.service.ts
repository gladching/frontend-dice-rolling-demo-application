import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DiceSimulatorService {

  constructor(
    private http:HttpClient,
  ) { }

  callDiceSimulator(diceCount, diceSides, rollCount) {
    const params = new HttpParams()
      .set('diceCount', diceCount)
      .set('diceSides', diceSides)
      .set('rollCount', rollCount);
    return this.http.post<any[]>('http://localhost:8080/dice-simulator', params)
  }

  retrieveRollDetails() {
    return this.http.get<any[]>('http://localhost:8080/dice-simulator/roll-details')
  }

  retrieveDistributionPercentages(diceCount, diceSides) {
    const params = new HttpParams()
    .append('diceCount', diceCount)
    .append('diceSides', diceSides);
    return this.http.get<any[]>('http://localhost:8080/dice-simulator/distribution-details',{params})
  }
}
