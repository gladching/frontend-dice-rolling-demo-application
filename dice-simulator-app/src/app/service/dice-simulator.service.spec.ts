import { TestBed } from '@angular/core/testing';

import { DiceSimulatorService } from './dice-simulator.service';

describe('DiceSimulatorService', () => {
  let service: DiceSimulatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiceSimulatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
