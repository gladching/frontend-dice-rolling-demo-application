import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { RollDetailsComponent } from './main/roll-details/roll-details.component';
import { RollDistributionComponent } from './main/roll-distribution/roll-distribution.component';
import { RollSimulatorComponent } from './main/roll-simulator/roll-simulator.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HttpParams } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RollDetailsComponent,
    RollDistributionComponent,
    RollSimulatorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule 
  ],
  providers: [HttpClient, HttpParams],
  bootstrap: [AppComponent]
})
export class AppModule { }
